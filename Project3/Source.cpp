#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <iomanip>
#include "Item.cpp"

using namespace std;

void printMenu();


int main(){
	/*
	Variable Declerations
	*/
	char dataChoice, menuChoice;
	vector<Item> inventory;
	string fileLocation;


	cout << "Would you like to enter data by hand (H) or read it from a file (F): ";
	cin >> dataChoice;
	dataChoice = tolower(dataChoice);
	while ((dataChoice) != 'h' && (dataChoice) != 'f'){
		cout << "Please enter a valid input of Hand (H) or File (F): ";
		cin >> dataChoice;
		dataChoice = tolower(dataChoice);
	}
	int numOfItems, itemId, amountOfItems;
	double sellPrice, buyPrice, storageFee;
	string itemName;
	if (dataChoice == 'h'){
		cout << "How many items would you like to add?" << endl;
		cin >> numOfItems;
		for (int i = 0; i < numOfItems; i++){
			cout << "---- Item " << (i + 1) << " ----" << endl;
			cout << "Item id:";
			cin >> itemId;
			cout << "Item Name:";
			cin >> itemName;
			cout << "Number of Items:";
			cin >> amountOfItems;
			cout << "Selling Price:";
			cin >> sellPrice;
			cout << "Buying Price:";
			cin >> buyPrice;
			cout << "Storage Fee:";
			cin >> storageFee;
			Item item(itemId, amountOfItems, buyPrice, sellPrice, storageFee, itemName);
			inventory.push_back(item);
		}
	}
	else{ //Item loaded from file
		cout << "File Location: ";
		cin >> fileLocation;
		ifstream input(fileLocation);
		if (input.fail()){
			cout << "Couldn't find the file";
		}
		else{
			while (input >> itemId){
				input >> itemName >> amountOfItems >> sellPrice >> buyPrice >> storageFee;
				Item item(itemId, amountOfItems, buyPrice, sellPrice, storageFee, itemName);
				inventory.push_back(item);
			}

		}
	}

	do{
		printMenu();
		cin >> menuChoice;
		menuChoice = tolower(menuChoice);
		if (menuChoice == 'a'){
			for (int i = 0; i < inventory.size(); i++){
				cout << "---- Item " << (i + 1) << " ----" << endl;
				cout << "Item Name: " << inventory[i].getName() << endl;
				cout << "Number of Items: " << inventory[i].getNumberOfItems() << endl;
				cout << "Profit: $" << fixed << setprecision(2) << inventory[i].getProfits() << endl;
				cout << "Selling Price: $" << fixed << setprecision(2) << inventory[i].getSellPrice() << endl;
				cout << "Buying Price: $" << fixed << setprecision(2) << inventory[i].getBuyPrice() << endl;
				cout << "Storage Fee: $" << fixed << setprecision(2) << inventory[i].getStorageFee() << endl;
			}
		}
		else if (menuChoice == 'b'){
			int totalNumberOfItems = 0;
			double totalProfit = 0, avgBuy = 0, avgSell = 0;
			for (int i = 0; i < inventory.size(); i++){
				totalNumberOfItems += inventory[i].getNumberOfItems();
				totalProfit += inventory[i].getProfits();
				avgBuy += inventory[i].getBuyPrice();
				avgSell += inventory[i].getSellPrice();
			}
			cout << "---- General Information ----" << endl;
			cout << "Total Number of Items: " << totalNumberOfItems << endl;
			cout << "Total Profit: $" << totalProfit << endl;
			cout << "Average Profit: $" << (totalProfit / inventory.size()) << endl;
			cout << "Average Buying Price: $" << (avgBuy / inventory.size()) << endl;
			cout << "Average Selling Price: $" << (avgSell / inventory.size()) << endl;
		}
		else if (menuChoice == 'c'){
			cout << left << "Item ID" << setw(13) << "Item Name" << setw(13)
				<< "Profit" << setw(13) << "Sell Price" << setw(13) << "Buy Price" << setw(13) << "Storage Fee" << endl;
			for (int i = 0; i < inventory.size(); i++){
				cout << inventory[i].getId() << setw(13) << inventory[i].getName() << setw(13)
					<< fixed << setprecision(2) << inventory[i].getProfits()
					<< setw(13) << inventory[i].getSellPrice() << setw(13) << inventory[i].getBuyPrice()
					<< setw(13) << inventory[i].getStorageFee() << endl;
			}
		}
		else if (menuChoice == 'd'){
			cout << "Please enter the item id of the item you would like to edit:" << endl;
			cin >> itemId;
			for (int i = 0; i < inventory.size(); i++){
				if (inventory[i].getId() == itemId){
					cout << "Please enter new values:";
					cout << "Item Name:";
					cin >> itemName;
					cout << "Number of Items:";
					cin >> amountOfItems;
					cout << "Selling Price:";
					cin >> sellPrice;
					cout << "Buying Price:";
					cin >> buyPrice;
					cout << "Storage Fee:";
					cin >> storageFee;
					inventory[i].setName(itemName);
					inventory[i].setBuy(buyPrice);
					inventory[i].setNumOfItems(amountOfItems);
					inventory[i].setSell(sellPrice);
					inventory[i].setStorage(storageFee);
					break;
				}
			}
		}
		else if (menuChoice == 'e'){
			if (fileLocation == "" || fileLocation.empty()){
				cout << "Please specify a filename: ";
				cin >> fileLocation;
			}
			ofstream output(fileLocation);
			if (output.fail()){
				cout << "Couldn't open file" << endl;
			}
			else{
				for (int i = 0; i < inventory.size(); i++){
					output << inventory[i].getId() << endl;
					output << inventory[i].getName() << endl;
					output << inventory[i].getNumberOfItems() << endl;
					output << inventory[i].getSellPrice() << endl;
					output << inventory[i].getBuyPrice() << endl;
				}
				output.close();
			}
		}

	} while (menuChoice != 'f');


	system("PAUSE");
	return 0;
}

void printMenu(){
	cout << "---- Menu ----" << endl;
	cout << "a. Print Items Information" << endl;
	cout << "b. Print General Information" << endl;
	cout << "c. Print Items Table" << endl;
	cout << "d. Edit an Item Information" << endl;
	cout << "e. Save information to file" << endl;
	cout << "f. Exit program" << endl;
}



