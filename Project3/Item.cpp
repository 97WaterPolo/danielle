#include <string>

using namespace std;


class Item{
public:
	Item(int cid, int cnumOfItems, double cbuyPrice, double csellPrice, double cstorageFee, string cname){
		id = cid;
		numOfItems = cnumOfItems;
		buyPrice = cbuyPrice;
		sellPrice = csellPrice;
		storageFee = cstorageFee;
		name = cname;
	}

	int getId(){
		return id;
	}

	int getNumberOfItems(){
		return numOfItems;
	}

	double getBuyPrice(){
		return buyPrice;
	}

	double getSellPrice(){
		return sellPrice;
	}

	double getStorageFee(){
		return storageFee; 
	}

	string getName(){
		return name;
	}

	double getProfits(){
		return (numOfItems)*(sellPrice - (buyPrice + storageFee));
	}

	void setName(string x){
		name = x;
	}
	void setNumOfItems(int x){
		numOfItems = x;
	}
	void setBuy(double x){
		buyPrice = x;
	}
	void setSell(double x){
		sellPrice = x;
	}
	void setStorage(double x){
		storageFee = x;
	}

private:
	int id, numOfItems;
	double buyPrice, sellPrice, storageFee;
	string name;
};


